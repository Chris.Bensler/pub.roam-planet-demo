#pragma once
#include <math.h>

#define PI 3.14159265f
#define PI_TWO (PI*2.0f)
#define PI_HALF (PI/2.0f)
#define RADIANS (PI/180.0f)
#define DEG2RAD RADIANS
#define RAD2DEG (1.0f/RADIANS)
#define DELTA   (1e-6f)

// common macros
#define MINCLAMP(min,x) ((x < min) ? min : x)
#define MAXCLAMP(max,x) ((x > max) ? max : x)
#define CLAMP(min,max,x) ((x < min) ? min : (x > max) ? max : x)

// newtonion physics macros
#define NEWTON_AVG_VELOCITY(displacement,time) ((displacement)/(time)) /* Avg Velocity = Displacement / Time */
#define NEWTON_AVG_ACCEL(velocity,time) ((velocity)/(time)) /* Avg Acceleration = Velocity Change / Time */
/* Displacement = (Vi + Vf)*t / 2 */
/* Vf = Vi + a*t */
/* d = Vi*t + (a*t^2)/2 */
/* Vf^2 = Vi^2 + 2*a*d */

/* 1 Newton = 1kg * 1m/s */
#define NEWTON_FORCE(mass,acceleration) ((mass)*(acceleration)) /* Force = Mass * Acceleration */
#define NEWTON_WEIGHT(mass,gravity) ((mass)*(gravity)) /* Weight = Mass*Gravity */
#define NEWTON_NORMAL_FORCE(mass,gravity) -((mass) * -(gravity))/* Normal Force = -(Mass*-Gravity) */
#define NEWTON_FRICTION_FORCE(friction,mass,gravity) ((friction)*NEWTON_NORMAL_FORCE()) /* Friction Force = Friction*Normal Force */

/* galactic measures */
#define GRAVCONST		6.67259e-20f	// Gravitational constant of the universe (km3 / kg sec2)

/* in km */
#define PARSEC 3.875e13f /* 30.875 trillion km */
#define ASTRONOMICAL_UNIT 1.49598e8f /* 150 million km, avg distance from sun to earth */
#define LIGHT_YEAR 9.4605284e12f /* 9.5 trillion */
#define LIGHT_MONTH (LIGHT_YEAR/12.0f) /* 788 billion */
#define LIGHT_WEEK (LIGHT_YEAR/52.0f) /* 182 billion */
#define LIGHT_DAY (LIGHT_YEAR/365.0f) /* 25.9 billion */
#define LIGHT_HOUR (LIGHT_DAY/24.0f) /* 1.1 billion */
#define LIGHT_MINUTE (LIGHT_HOUR/60.0f) /* 18 million */
#define LIGHT_SECOND (LIGHT_MINUTE/60.0f) /* 300,000 */
#define LIGHTSPEED LIGHT_SECOND

//             Distance  Radius    Mass
//             (000 km)    (km)     (kg)
// ---------  ---------  ------  -------
// Sun                0  695500  1.99e30
// Mercury       57,910    2439  3.30e23
// Venus        108,200    6052  4.87e24
// Earth        149,600    6378  5.98e24
// Moon         384.400    1738  7.35e22 // relative to earth!
// Mars         227,940    3397  6.42e23
// Jupiter      778,330   71492  1.90e27
// Saturn     1,426,940   60268  5.69e26
// Uranus     2,870,990   25559  8.69e25
// Neptune    4,497,070   24764  1.02e26
// Pluto      5,913,520    1160  1.31e22

namespace Metric {
    //                  1e16      1e12      1e9       1e6       1e3       1e0   1e-2        1e-3
    typedef const float Lightyear,Terametre,Gigametre,Megametre,Kilometre,Metre,Centimetre,Millimetre;
    //                  1e15      1e12      1e9       1e6       1e3       1e0   1e-2       1e-3
    typedef const float Gigatonne,Megatonne,Kilotonne,Tonne,    Kilogram, Gram, Centigram, Milligram;
}

namespace Stellar {
    namespace MilkyWay          { const float Stars=   1.0e11f;        Metric::Lightyear Radius= 50000.0f; } // relative to the universe
    //                                                                                                                                                          m/s^2
    namespace Sun               { Metric::Lightyear Orbit=    2.64e4f; Metric::Kilometre Radius= 695500.0f; Metric::Megatonne Mass=     1.99e21f; Metric::Metre Gravity= 274.40f; } // relative to the galaxy
            namespace Mercury   { Metric::Kilometre Orbit=   57.91e6f; Metric::Kilometre Radius=   2440.0f; Metric::Megatonne Mass=    33.00e13f; } // relative to the sun
            namespace Venus     { Metric::Kilometre Orbit=  108.20e6f; Metric::Kilometre Radius=   6052.0f; Metric::Megatonne Mass=   487.00e13f; } // relative to the sun
            namespace Earth     { Metric::Kilometre Orbit=  149.60e6f; Metric::Kilometre Radius=   6378.0f; Metric::Megatonne Mass=   598.00e13f; Metric::Metre Gravity=   9.80f;} // relative to the sun
                namespace Moon  { Metric::Kilometre Orbit=  384.40e3f; Metric::Kilometre Radius=   1738.0f; Metric::Megatonne Mass=   735.00e11f; Metric::Metre Gravity=   1.63f;} // relative to the earth
            namespace Mars      { Metric::Kilometre Orbit=  227.94e6f; Metric::Kilometre Radius=   3398.0f; Metric::Megatonne Mass=    64.20e13f; } // relative to the sun
            namespace Jupiter   { Metric::Kilometre Orbit=  778.33e6f; Metric::Kilometre Radius=  71492.0f; Metric::Megatonne Mass=190000.00e13f; } // relative to the sun
            namespace Saturn    { Metric::Kilometre Orbit= 1426.94e6f; Metric::Kilometre Radius=  60268.0f; Metric::Megatonne Mass= 56900.00e13f; } // relative to the sun
            namespace Uranus    { Metric::Kilometre Orbit= 2870.99e6f; Metric::Kilometre Radius=  25560.0f; Metric::Megatonne Mass=  8690.00e13f; } // relative to the sun
            namespace Neptune   { Metric::Kilometre Orbit= 4497.07e6f; Metric::Kilometre Radius=  24764.0f; Metric::Megatonne Mass= 10200.00e13f; } // relative to the sun
            namespace Pluto     { Metric::Kilometre Orbit= 5913.52e6f; Metric::Kilometre Radius=   1160.0f; Metric::Megatonne Mass=     1.31e13f; } // relative to the sun
}
