#pragma once
#include "Noise.h"
#include "assert.h"

namespace Fractal {

// Defines and interpolates between a range of gradients for a given height
class CSpectrum {
private:
    CVector4f *lpSpectrum;
    uint8 size;
    uint8 count;
public:
    CSpectrum(uint32 size) {
        this->lpSpectrum = (CVector4f*)malloc(sizeof(CVector4f)*size);
        this->size = size;
        this->count =  0;
    }
    ~CSpectrum() {
        free(this->lpSpectrum);
    }

    // Add a new gradient color for the specified height
    // rgb values are absolute scalar format (0.0 to 1.0)
    // height is a point within a user defined (implicit) range
    void AddGradient(float32 r, float32 g, float32 b, float32 height) {
        if (this->count >= this->size) return;
        // insert sorted by height in ascending order
        int i = count-1;
        count++;
        for (; i >=0; i--) {
            if (this->lpSpectrum[i].w <= height) break;
            this->lpSpectrum[i+1] = this->lpSpectrum[i];
        }
        this->lpSpectrum[i+1].Set(r,g,b,height);
    }
    // Add a new gradient color for the specified height
    // rgb values are absolute scalar format (0.0 to 1.0)
    // height is a point within a user defined (implicit) range
    void AddGradient(CVector4f rgbh) { AddGradient(rgbh.x,rgbh.y,rgbh.z,rgbh.w); }
    // Add a new gradient color for the specified height
    // rgb values are unsigned byte format (0 to 255)
    // height is a point within a user defined (implicit) range
    void AddGradient256(uint8 r, uint8 g, uint8 b, float32 height) { AddGradient(r/255.0f,g/255.0f,b/255.0f,height); }
    // Add a new gradient color for the specified height
    // rgb values are unsigned byte format (0 to 255)
    // height is a point within a user defined (implicit) range
    void AddGradient256(CVector4f rgbh) { AddGradient256((uint8)rgbh.x,(uint8)rgbh.y,(uint8)rgbh.z,rgbh.w); }

    // Get the interpolated color gradient for the specified height value
    // returns an rgb vector in scalar format
    const CVector3f GetGradient(float32 height) {
        float32 hMin,hMax;
        CVector3f rgbMin,rgbMax;
        height = CLAMP(-1,1,height);
        for (uint8 i=1; i < this->count; i++) {
            hMax = this->lpSpectrum[i].w;
            if (height <= hMax) {
                hMin = this->lpSpectrum[i-1].w;
                height = (height-hMin) / (hMax - hMin);
                // perturb height by ratio (scale-abs(x)*scale)*exp(0-abs(x)/scale*strength)
                rgbMin = *(CVector3f*)&(this->lpSpectrum[i-1]);
                rgbMax = *(CVector3f*)&(this->lpSpectrum[i]);
                rgbMax = (rgbMax-rgbMin) * height;
                return rgbMin + rgbMax;
            }
        }
        return NULL;
    }
};

extern int Octaves;
extern int Ridged;
extern float64 Factor;
extern float64 Frequency, Gain;
extern float64 Amplitude, Slope;

float64 fbM(CVector3 *v, int octaves, float64 freq, float64 gain, float64 amp, float64 slope, float64 factor=0);
float64 fbMRidged(CVector3 *v, int octaves, float64 freq, float64 gain, float64 amp, float64 slope, float64 factor=0, int ridged=1);

// radius is the size of the planet
float32 GetHeight(CVector3 *vert, float32 radius, float32 altitude);

} // namespace Fractal