#pragma once
#include "Types.h"

class CRandom {
protected:
    uint32 seed;

public:
    CRandom() { this->SetSeed(0); }
    CRandom(uint32 seed) { this->SetSeed(seed);}

    void SetSeed(uint32) { this->seed = seed; srand(seed); }
    uint32 GetSeed() { return this->seed; }

    uint32 GetInt() { return rand(); }
    float32 GetFloat() { return (float32)this->GetInt()/RAND_MAX; }

    float32 GetFloat(float32 max) { return (float32)(this->GetFloat()*max); }
    uint32 GetInt(uint32 max) { return (uint32)this->GetFloat((float32)max); }
};