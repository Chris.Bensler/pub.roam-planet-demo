#pragma once
#include <stdlib.h>

template <class T> class TListNode {
public:
    T* next;
    T* prev;

    TListNode() { next = prev = NULL; }
    ~TListNode() { Remove(); }

    // add a new node after this node
    void InsertNext(T* node) {
        node->prev = (T*)this;
        node->next = next;
        if (next) next->prev = node;
        next = node;
    }

    // add a new node before this node
    void InsertPrev(T* node) {
        node->next = (T*)this;
        node->prev = prev;
        if (prev) prev->next = node;
        prev = node;
    }

    void Remove() {
        if (prev) prev->next = next;
        if (next) next->prev = prev;
        next = prev = NULL;
    }
};

template <class T> class TLinkedList {
public:
    TListNode<T> head;
    TListNode<T> tail;

    TLinkedList() { head.prev = NULL; head.next = (T*)&tail; tail.prev = (T*)&head; tail.next = NULL; }
    ~TLinkedList() { this->RemoveAll(); }

    int Count() {
        int count = 0;
        for (TListNode<T>* node = head.next; node != &tail; node = node->next) count++;
        return count;
    }

    void InsertFirst(T *node) { this->head.InsertNext(node); }
    void InsertLast(T *node) { this->tail.InsertPrev(node); }

    T* GetFirst() { return this->head.next; }
    T* GetLast() { return this->tail.prev; }

    bool IsHead(T *node) { return (node->prev == NULL); }
    bool IsTail(T *node) { return (node->next == NULL); }
    bool IsNode(T *node) { return !(IsHead(node) || IsTail(node)); }

    void RemoveAll() { while (head.next != (T*)&tail) delete head.next; }
};