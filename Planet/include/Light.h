#pragma once
#include "Global.h"

class CLight {
public:
    CVector4f Position,Ambient,Diffuse,Specular;
    
    CLight() { CLight(0.0f, 0.0f, 1.0f, 0.0f); }
    CLight(float32 x, float32 y, float32 z, float32 w) {
        this->Position.Set(x,y,z,w);
        this->Ambient.Set( 0.2f, 0.2f, 0.2f, 1.0f);
        this->Diffuse.Set( 1.0f, 1.0f, 1.0f, 1.0f);
        this->Specular.Set(1.0f, 1.0f, 1.0f, 1.0f);
    }
};