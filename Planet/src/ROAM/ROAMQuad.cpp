#include "ROAM/ROAMQuad.h"

ROAM::CQuadNode::CQuadNode(CTriNode *tri) {
    this->parent = tri;
    this->parent->quad = this;
    this->parent->edge[1]->quad = this;
    this->parent->edge[1]->edge[1]->quad = this;
    this->parent->edge[1]->edge[1]->edge[1]->quad = this;
    // set the priority info
}

ROAM::CQuadNode::~CQuadNode() {
    if (!this->parent) return;
    if (!this->parent->quad) return;
    this->parent->edge[1]->edge[1]->edge[1]->quad = NULL;
    this->parent->edge[1]->edge[1]->quad = NULL;
    this->parent->edge[1]->quad = NULL;
    this->parent->quad = NULL;
    this->parent = NULL;
}
