#include "ROAM/ROAMTri.h"

CVector ROAM::CTriNode::GetMidPoint(CSphere *sphere) {
    return sphere->VertList[this->vert[1]]->pos.Midpoint(sphere->VertList[this->vert[2]]->pos);
}

CVector3 ROAM::CTriNode::GetNormal(CSphere *sphere) {
    return sphere->VertList[this->vert[0]]->pos.NormalVector(sphere->VertList[this->vert[1]]->pos,sphere->VertList[this->vert[2]]->pos);
}

void ROAM::CTriNode::UpdateNormal(CSphere *sphere, uint16 index) {
	// Go counter-clockwise through neighbours to add up normals of all triangles using this vertex
    CVertex *v = sphere->VertList[this->vert[index]];
	v->normal = this->GetNormal(sphere);
	CTriNode *p = this;
	CTriNode *n = this->edge[index];
	while(n != this) {
		v->normal += n->GetNormal(sphere);
		int nn = (n->edge[1] == p) ? 2 : n->edge[2] == p ? 1 : 0;
		p = n; n = n->edge[nn];
	}
}

void ROAM::CTriNode::UpdatePriority(CSphere *sphere) {
    this->length = (float32)( sphere->VertList[this->vert[1]]->pos.SquareDelta(sphere->VertList[this->vert[2]]->pos) );
    this->midpoint = this->GetMidPoint(sphere);
    float64 mag = this->midpoint.Magnitude();
    this->target = CVector(this->midpoint).Normalize();
    float64 h = this->height = Fractal::GetHeight(&CVector3(this->target),sphere->Scale,MAX_ALTITUDE);
    if (h < 0) h = 0;
    this->target = CVector(this->midpoint) * (float64)((h + sphere->Scale)/mag);
    this->offset = (float64) (this->target.Magnitude() - mag);
}

void ROAM::CTriNode::CopyPriority(CSphere *sphere) {
    if (this->CanSplit()) {
        CPriority::CopyPriority(this->edge[0]);
    } else {
        this->UpdatePriority(sphere);
    }
}
