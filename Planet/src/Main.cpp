#include "Main.h"

sf::RenderWindow App;
uint32 WindowStyle = sf::Style::Resize | sf::Style::Close;

std::ostringstream textBuffer;
sf::String textString;
bool ShowDebug = true;

C3DObject Camera;
CLight Light0;
C3DObject Sun;
sf::Image bmpSun1,bmpSun2;
ROAM::CSphere Earth;

uint32 ScreenW = SCREEN_W, ScreenH = SCREEN_H;
bool UpdateFlag = true;
float32 MAX_ERROR = 0.0001f;
float32 frameError = MAX_ERROR;

int Fractal::Octaves = 15;
int Fractal::Ridged = 5;
float64 Fractal::Factor = 1;
float64 Fractal::Frequency = 1;
float64 Fractal::Gain = 2;
float64 Fractal::Amplitude = 1;
float64 Fractal::Slope = 0.5;

GLuint VBO = 0;

bool InitOpenGL() {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glDepthFunc(GL_LESS);
    glShadeModel(GL_SMOOTH);
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE); // required for scaled matrices, maybe better to set per-object at render time?
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    Light0 = CLight(0,1.5,1.5,TRUE);
    glLightfv(GL_LIGHT0,GL_POSITION,(float32*)&Light0.Position);
    glLightfv(GL_LIGHT0,GL_AMBIENT,(float32*)&Light0.Ambient);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,(float32*)&Light0.Diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,(float32*)&Light0.Specular);
    glEnable(GL_LIGHT0);

    //If there was any errors
    if (glGetError() != GL_NO_ERROR) {
        std::cerr << "Failed to initialize openGL. Error Code: " << glGetError() << std::endl;
        return false;
    }
    return true;
}

void UpdateViewport(uint32 w, uint32 h) {
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION); glLoadIdentity();
    gluPerspective(FOVY,(float)w/(float)h,ZNEAR,ZFAR);
    glMatrixMode(GL_MODELVIEW);
}

void SetVertexBuffer(CVertex *buffer) {
    if (VBO) {
        glBindBufferARB(GL_ARRAY_BUFFER_ARB,VBO);
        glBufferDataARB(GL_ARRAY_BUFFER_ARB,sizeof(CVertex)*MAX_VERTS,buffer,GL_STREAM_DRAW_ARB);
        glVertexPointer(3,GL_FLOAT,sizeof(CVertex),((uint8*)NULL + (sizeof(CVector3)*0)));
        glNormalPointer(  GL_FLOAT,sizeof(CVertex),((uint8*)NULL + (sizeof(CVector3)*1)));
        glColorPointer(3, GL_FLOAT,sizeof(CVertex),((uint8*)NULL + (sizeof(CVector3)*2)));
    } else {
        glVertexPointer(3,GL_FLOAT,sizeof(CVertex),&(buffer[0].pos));
        glColorPointer(3,GL_FLOAT,sizeof(CVertex),&(buffer[0].rgb));
        glNormalPointer(GL_FLOAT,sizeof(CVertex),&(buffer[0].normal));
    }
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
}

const CVector2i MouseOffset(const CVector2i& mNew) {
  static CVector2i mOld;
  static bool once = true;
  CVector2i mTmp;
    if (once) { once = false; mOld = mNew; }
    mTmp = mOld; mOld = mNew;
    return CVector2i(mNew - mTmp);
}

void Poll() {
// Octaves    - +
// Frequency  4 7
// Gain       SHIFT 4 7
// Amplitude  6 9 
// Slope      SHIFT 6 9
// Factor     2 8
// Ridged     1 3

    sf::Event Event;
    while (App.GetEvent(Event)) {
        sf::Event::EventType e = Event.Type;
        if (e == sf::Event::Closed) {
            App.Close();
        } else if (e == sf::Event::Resized) {
            uint32 w = Event.Size.Width, h = Event.Size.Height;
            ((sf::View&)App.GetView()).SetFromRect(sf::FloatRect(0,0,(float)w,(float)h));
            UpdateViewport(w,h);
        } else if (e == sf::Event::KeyPressed) {
            sf::Key::Code k = Event.Key.Code;
            if (k == sf::Key::Escape) {
                App.Close();
            } else if (Event.Key.Alt && (k == sf::Key::Return)) { // toggle fullscreen
                uint32 w = ScreenW, h = ScreenH;
                if (WindowStyle & sf::Style::Fullscreen) {
                    WindowStyle = (sf::Style::Resize | sf::Style::Close);
                } else {
                    WindowStyle = sf::Style::Fullscreen;
                    ScreenW = App.GetWidth();   ScreenH = App.GetHeight(); // save old window state
                    // hack, use desktop resolution for fullscreen mode
                    w = sf::VideoMode::GetDesktopMode().Width;
                    h = sf::VideoMode::GetDesktopMode().Height;
                }
                App.Create(sf::VideoMode(w,h),APP_TITLE,WindowStyle);
                if (!InitOpenGL()) { App.Close(); return; }
                UpdateViewport(w,h);
            } else if (k == sf::Key::F) { // fill mode
                GLint params[2];
                glGetIntegerv(GL_POLYGON_MODE,(GLint*)params);
                params[0] = (params[0] == GL_FILL) ? GL_LINE : GL_FILL;
                glPolygonMode(GL_FRONT_AND_BACK,params[0]);
            } else if (k == sf::Key::U) { // toggle updates on/off
                UpdateFlag = !UpdateFlag;
            } else if (k == sf::Key::D) { // debug info
                ShowDebug = !ShowDebug;
            } else if (k == sf::Key::LBracket) {
                frameError *= 1.1f;
            } else if (k == sf::Key::RBracket) {
                frameError /= 1.1f;
            } else if (k == sf::Key::Add) {
                if (Fractal::Octaves < 15) Fractal::Octaves++;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Subtract) {
                if (Fractal::Octaves > 1) Fractal::Octaves--;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad7) {
                if (Event.Key.Control) Fractal::Gain *= 1.1; else Fractal::Frequency *= 1.1;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad4) {
                if (Event.Key.Control) Fractal::Gain /= 1.1; else Fractal::Frequency /= 1.1;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad9) {
                if (Event.Key.Control) Fractal::Slope *= 1.1; else Fractal::Amplitude *= 1.1;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad6) {
                if (Event.Key.Control) Fractal::Slope /= 1.1; else Fractal::Amplitude /= 1.1;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad5) {
                Fractal::Factor = (Fractal::Factor == 0);
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad8) {
                Fractal::Factor *= 1.1;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad2) {
                Fractal::Factor /= 1.1;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad3) {
                if (Fractal::Ridged < 15) Fractal::Ridged++;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::Numpad1) {
                if (Fractal::Ridged > 0) Fractal::Ridged--;
                Earth.Init(Stellar::Earth::Radius);
            } else if (k == sf::Key::V) {
                if (VBO) { // disable
                    glDeleteBuffersARB(1,&VBO);
                    VBO = 0;
                } else if (GLEE_ARB_vertex_buffer_object) { // enable
                    glGenBuffersARB(1,&VBO);
                }
            }
        } else if (e == sf::Event::MouseButtonPressed) {
            if (Event.MouseButton.Button == sf::Mouse::Right) { // begin camera scan
                MouseOffset(CVector2i(Event.MouseButton.X, Event.MouseButton.Y)); // update origin
                App.ShowMouseCursor(false); // hide the mouse cursor while scanning
            }
        } else if (e == sf::Event::MouseButtonReleased) {
            if (Event.MouseButton.Button == sf::Mouse::Right) { // end camera scan
                App.ShowMouseCursor(true); // show the mouse cursor
            }
        }
    }

    // direct input
    const sf::Input *Input = &App.GetInput();
    if (Input->IsMouseButtonDown(sf::Mouse::Right)) { // scan
        CVector2f offset = MouseOffset( CVector2i(Input->GetMouseX(), Input->GetMouseY()) );
        offset *= -App.GetFrameTime();

        CVector3 pitch = Camera.Rot.GetRightAxis();
        CVector3 yaw   = Camera.Rot.GetUpAxis();
        if (Input->IsMouseButtonDown(sf::Mouse::Left)) { // rotate camera
            Camera.Rot.Rotate(pitch,offset.y); // pitch
            Camera.Rot.Rotate(yaw,  offset.x); // yaw
        } else { // rotate earth
            pitch = CVector3(Earth.Rot.RotateVector(pitch)).Normalize();
            yaw   = CVector3(Earth.Rot.RotateVector(yaw)).Normalize();
            Earth.Rot.Rotate(pitch, offset.y); // pitch
            Earth.Rot.Rotate(yaw,   offset.x); // yaw
        }
    }
    if (Input->IsKeyDown(sf::Key::Q)) { // roll
        Camera.Rot.Rotate(Camera.Rot.GetViewAxis(), App.GetFrameTime() * -2.0f);
    } else if (Input->IsKeyDown(sf::Key::E)) {
        Camera.Rot.Rotate(Camera.Rot.GetViewAxis(), App.GetFrameTime() * +2.0f);
    }
    float32 speed = 1;
    if (Input->IsKeyDown(sf::Key::LShift)) speed *= 10;
    if (Input->IsKeyDown(sf::Key::LControl)) speed *= 100;
    if (Input->IsKeyDown(sf::Key::LAlt)) speed = 1.0f/(10*speed);
    speed *= 1/(1-App.GetFrameTime());

    if (Input->IsKeyDown(sf::Key::W)) { // dolly
        Camera.Pos += Camera.Rot.RotateVector(CVector(0,0, -speed));
    } else if (Input->IsKeyDown(sf::Key::S)) {
        Camera.Pos += Camera.Rot.RotateVector(CVector(0,0, +speed));
    }
}

void Update() {
    if (!UpdateFlag) return;
    CVector position = (Camera.Pos - Earth.Pos);
    CQuaternion axis = Earth.Rot; //.UnitInverse();
    CQuaternion heading = Camera.Rot;
    position = axis.RotateVector(position);
    heading.Rotate(axis);
    double radius = Stellar::Earth::Radius;
    double altitude = position.Magnitude() - radius;
    if (altitude < 1.0f) altitude = 1.0f;
    float32 horizon = (float32) ( altitude*altitude + 2.0f*altitude*radius );
    Earth.Update(position, heading.GetViewAxis(), horizon, frameError);
}

// draw a textured quad. z = 0
void DrawQuad(const CVector4f &rect = CVector4f(-1,-1,+1,+1), const CVector4f &uv = CVector4f(0,0,1,1)) {
    glBegin(GL_QUADS);
        glTexCoord2f(uv[0],uv[3]); glVertex2f(rect[0],rect[1]); // lt
        glTexCoord2f(uv[0],uv[1]); glVertex2f(rect[0],rect[3]); // lb
        glTexCoord2f(uv[2],uv[1]); glVertex2f(rect[2],rect[3]); // rb
        glTexCoord2f(uv[2],uv[3]); glVertex2f(rect[2],rect[1]); // rt
    glEnd();
}
void DrawQuad(float scale, const CVector4f &uv = CVector4f(0,0,1,1)) {
    DrawQuad(CVector4f(-scale,-scale,+scale,+scale),uv);
}

void RenderFar() {
    // draw the sun
    CVector3d v = (Sun.Pos - Camera.Pos);
    float fade = (float)((v.Magnitude()-20.0e6f)/50.0e6f);
    fade = (fade < 0.0f) ? 0.0f : (fade > 1.0f) ? 1.0f : fade;
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPushMatrix();
        glMultMatrixf(Sun.GetScaledModelMatrix(v, Stellar::Sun::Radius*4));
        bmpSun2.Bind(); glColor3f(fade,fade,fade*200/255);  DrawQuad(15);
        bmpSun1.Bind(); glColor3f(1.0f,1.0f,1.0f);          DrawQuad(1);
    glPopMatrix();
    glPopAttrib();
}

void RenderDepth() {
    // draw the earth
    SetVertexBuffer(Earth.VertList.GetBuffer());
    glPushMatrix();
        glMultMatrixf(Earth.GetScaledModelMatrix(&Camera,1.0f));
        glDrawElements(GL_TRIANGLES,Earth.TCount,GL_UNSIGNED_SHORT,Earth.Index);
    glPopMatrix();
}

void RenderNear() {
    // debug text
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    textBuffer << "FPS: " << (uint32)(1.0f/App.GetFrameTime()) << std::endl;
    textString.SetText(textBuffer.str()); textString.SetPosition(10,10); App.Draw(textString);
    glPopAttrib();

    // reset for NEXT frame
    textBuffer.str("");
    if (ShowDebug) {
        textBuffer << "Update: " << ((UpdateFlag) ? "ON" : "OFF") << ", Threshold: " << frameError << std::endl;
        //textBuffer << "Vert: " << Earth.VertList.GetTotal() << ", Stack: " << Earth.VertList.GetUnused() << ", Used: " << Earth.VertList.Length() << std::endl;
        textBuffer << "Distance: " << abs(Camera.Pos.Delta(Earth.Pos)-Stellar::Earth::Radius) << "km" << std::endl;
        textBuffer << "Tri:  " << Earth.TriList.Count() << std::endl;
        //textBuffer << "Quad: " << Earth.QuadList.Count() << std::endl;
        //textBuffer << "Distance to Sun:   " << Camera.Pos.Magnitude() << "km" << std::endl;
        textBuffer << "Octaves: " << Fractal::Octaves << ", Ridged: " << Fractal::Ridged << ", Factor: " << Fractal::Factor << std::endl;
        textBuffer << "Frequency (Gain): " << Fractal::Frequency << " (" << Fractal::Gain << ")" << std::endl;
        textBuffer << "Amplitude (Slope): " << Fractal::Amplitude << " (" << Fractal::Slope << ")" << std::endl;
        textBuffer << "VBO Mode: " << ((VBO) ? "ON" : "OFF") << std::endl;
    }
}

void Render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadMatrixf(Camera.GetViewMatrix());

    RenderFar();
    RenderDepth();
    RenderNear();

    App.Display();
}

int main() {
    std::cout << "STDOUT TEST" << std::endl;

    SimplexNoise::Init();

    App.Create(sf::VideoMode(ScreenW,ScreenH),APP_TITLE);
    App.PreserveOpenGLStates(true);

    textString.SetSize(14.0f);

    if (!InitOpenGL()) return EXIT_FAILURE;

    if (!bmpSun1.LoadFromFile("bmp/sun1.dds")) { std::cerr << "Failed to load sun1.dds!"; return EXIT_FAILURE; }
    if (!bmpSun2.LoadFromFile("bmp/sun2.dds")) { std::cerr << "Failed to load sun2.dds!"; return EXIT_FAILURE; }

    Earth.Spectrum = new Fractal::CSpectrum(9);
    Earth.Spectrum->AddGradient256(  0,  0, 96,-1.00f); // min altitude
    Earth.Spectrum->AddGradient256(  0,  0,128,-0.35f); // deep water
    Earth.Spectrum->AddGradient256(  0,  0,196,-0.05f); // shallow water
    Earth.Spectrum->AddGradient256(244,244,210, 0.00f); // beach // no transition
    Earth.Spectrum->AddGradient256(120,154,120, 0.10f); // beach
    Earth.Spectrum->AddGradient256( 33, 67, 24, 0.30f); // trees
    Earth.Spectrum->AddGradient256( 96, 96, 96, 0.70f); // rock
    Earth.Spectrum->AddGradient256(192,192,192, 0.85f); // alps
    Earth.Spectrum->AddGradient256(225,225,225, 1.00f); // max altitude

    Earth.Init(Stellar::Earth::Radius);
    if (GLEE_ARB_vertex_buffer_object) glGenBuffersARB(1,&VBO);

    // start with the sun as the origin, units in km
    Sun.Pos.Set   (0.0f, 0.0f, 0.0f);
    Earth.Pos.Set (0.0f, 0.0f, Stellar::Earth::Orbit);
    Camera.Pos.Set(0.0f, 0.0f, Stellar::Earth::Orbit + (Stellar::Moon::Orbit/10));

    UpdateViewport(App.GetWidth(),App.GetHeight());
    glLoadIdentity();

    //float32 fps;
    while (App.IsOpened()) {
        Poll();
        Update();
        Render();
        //fps = 30 * App.GetFrameTime();
        //if (fps > 1.0f) {
        //    if (frameError < 2) frameError *= 1.01f;
        //} else {
        //    if (frameError > 1.0e-10) frameError /= 1.05f;
        //}
    }

    return EXIT_SUCCESS;
}