#include "Math/Fractal.h"

float64 Fractal::fbM(CVector3 *v, int octaves, float64 freq, float64 gain, float64 amp, float64 slope, float64 factor) {
  float64 val = 0, step = 0;
  float64 x = v->x, y = v->y, z = v->z;
    for (int o=0; o < octaves; o++) {
        step = (float32)SimplexNoise::Noise(x*freq,y*freq,z*freq);
        if (factor && o) step *= val*factor;
        val += step * amp;
        freq *= gain; amp *= slope;
    }
    return val;
}

float64 Fractal::fbMRidged(CVector3 *v, int octaves, float64 freq, float64 gain, float64 amp, float64 slope, float64 factor, int ridged) {
  float64 val = 0, step = 0;
  float64 x = v->x, y = v->y, z = v->z;
    if ((ridged < 1) || (ridged > octaves)) ridged = octaves+1; // invalid
    for (int o=0; o < octaves; o++) {
        step = (float32)SimplexNoise::Noise(x*freq,y*freq,z*freq);
        if (o && (o >= ridged)) step = ((step > 0) ? -step : step) + 1;
        if (factor && (o > ridged)) step *= val*factor;
        val += step * amp;
        freq *= gain; amp *= slope;
    }
    return val;
}

// scale is an abs scalar value 0.0 > to <= 1.0 and controls the detail level of the height value.
// radius is the size of the planet
float32 Fractal::GetHeight(CVector3 *vert, float32 radius, float32 altitude) {
  float64 height;
  float64 h1;//,h2,h3,h4;
    //h1 = fbMRidged((CVector3*)&(*vert), 7, 0.75, 3, 0.75, 0.6, 1, 2);
    //h1 *= fbMRidged(&CVector3(vert->y,vert->z,vert->x), 7, 0.75, 3, 0.75, 0.6, 1, 2);
    //h1 = (0 < fbMRidged((CVector3*)&(*vert), 3, 1.25, 2.5, 0.01, 0.6, 0, 0)); // true if land
    //h1 *= (0 < fbMRidged(&CVector3(vert->y,vert->z,vert->x), 3, 1.5, 4.5, 0.01, 0.6, 0, 0));
    ////h1 = h1*2 - 1;
    //h1 *= fbMRidged((CVector3*)&(*vert), 4, 1.5, 5, 0.5, 0.6, 1, 1)/2 + 0.5f;

    //h1 *= (0 > fbMRidged(&CVector3(vert->y,vert->z,vert->x), 3, 0.25, 4.5, 0.01, 0.3, 0, 0)) * 0.01;
    h1 = fbMRidged((CVector3*)&(*vert), Octaves, Frequency, Gain, Amplitude, Slope, Factor, Ridged);
    //h1 = SimplexNoise::Noise(vert->x,vert->y,vert->z);
    //h2 = fbMRidged((CVector3*)&(*vert*2.0f),6,0.3);
    //h1 = fbM((CVector3*)&(*vert*1.0f),1,0.7);
    //h1 = fbM((CVector3*)&(*vert*0.25f),12,0.7);
    //h1 = cos(vert->x + fbM((CVector3*)&(*vert*1.0f),12,0.7));
    //h2 = MAXCLAMP(1.0f,cos(vert->x + abs(fbM((CVector3*)&(*vert*5.0f),3,0.3))) / PI_HALF * 1.5f); 
    //h3 = CLAMP(-0.25f,0.25f,fbM((CVector3*)&(*vert*0.5f),5,0.1));
    height = h1;//+h2;// + h3;
    if (height < 0) {
        //height *= 0.5f;
        //height *= exp(height)/exp(1.0f)*4; // flatten
    } else {
        //height = exp(height)/exp(1.0f); // flatten
        //h4 = fbM((CVector3*)&(*vert*14),3,0.5);
        //h4 = (exp(h4) - exp(-1.0f)) / (exp(1.0f) - exp(-1.0f));
        //height *= h4;
    }
    return (float32)( altitude * height );
}
