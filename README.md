# ROAM Planet Generator Demo
  
**Author:** Chris Bensler


## Description/Notes
This is a prototype for a 3D planet generator using the ROAM algorithm for dynamic LOD, based on Gamasutra articles written by Sean O’Neil (Parts [1](1), [2](2) and [3](3)).  
Height-mapping is done using an implementation of the simplex noise algorithms (2D, 3D and 4D) with fractal-brownian motion.  
A custom vector and matrix math library using C++ templates was also produced for personal preferences.  

Unfortunately, I have not had time to work on this for a while and the project has been left imcomplete.  
As well, the ROAM algorithm is not efficient enough.  

I have learned about and would like to redo the planet generator using the Chunked Quad-Tree LOD algorithm when I find the time.  
Sean has released a newer article (Part [4](4)) discussing the issues with the ROAM algorithm and describing the Chunked LOD algorithm.  

-----------------------------
![Preview Image][img-preview]
-----------------------------  

## Requirements
- SFML Version 1.6 (library files included)  
- Compiled with Visual Studio 2008 but it shouldn't be difficult to rebuild the solution for newer versions.  


## Copyright Notice
This code is available as public domain. Feel free to use this code as you see fit.  
You may not remove the copyright notices, disclaimers or attribution in any of the files.


## Disclaimer
This code is released as-is. Don't blame me if your system blows up. It is expected that you know what you are doing.  
Please don't send me any questions regarding this code. It is unsupported, unfit for production and should only be used for reference.


[1]: http://www.gamasutra.com/view/feature/3098/a_realtime_procedural_universe_.php
[2]: http://www.gamasutra.com/view/feature/131451/a_realtime_procedural_universe_.php
[3]: http://www.gamasutra.com/view/feature/131393/a_realtime_procedural_universe_.php
[4]: http://www.gamasutra.com/view/feature/2511/a_realtime_procedural_universe_.php

[img-preview]: preview.png